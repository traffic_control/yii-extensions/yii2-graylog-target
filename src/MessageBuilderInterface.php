<?php

namespace credy\graylog;

use Gelf\Message;

/**
 * Interface MessageBuilderInterface
 *
 * @package credy\graylog
 */
interface MessageBuilderInterface
{
    /**
     * @param Target $target
     * @param array $message
     *
     * @return Message
     */
    public function build(Target $target, $message);
}
